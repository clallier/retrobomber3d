package src;

import com.haxepunk.Entity;
import com.haxepunk.graphics.Emitter;
import com.haxepunk.graphics.Image;
import com.haxepunk.graphics.Stamp;
import com.haxepunk.HXP;
import com.haxepunk.utils.Draw;
import flash.display.BitmapData;

import flash.geom.Rectangle;
import src.Player;
//import sys.io.File;
//import flash.system.System;

/**
 * ...
 * @author Godjam - QilinEggs
 */
class RayCast extends Entity
{
	//from http://dev.opera.com/articles/view/creating-pseudo-3d-games-with-html-5-can-1/
	// written by nihilogic

	// a 32x24 block map
	public var map(default, null):Array<Array<Int>>;
	public var objectsMap(default, null):Array<Array<Entity>>;
	public var mapWidth(default, null):Int  ;// number of map blocks in x-direction
	public var mapHeight(default, null):Int ;  // number of map blocks in y-direction
	var bg:Stamp;
	var strips:Array<Entity>;

	var twoPI:Float;
	var numRays:Int;
	var stripWidth:Int;
	var fov:Float;
	var fovHalf:Float;
	public var viewDist:Float;
	
	override public function added() 
	{
		//super(0, 0);
		
		map = [
		[1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1],
		[1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1],
		[1,0,1,2,1,0,1,0,1,2,1,2,1,2,1,2,1],
		[1,0,0,2,0,0,0,0,2,2,2,2,2,2,2,2,1],
		[1,0,1,2,1,0,1,0,1,0,1,0,1,0,1,0,1],
		[1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1],
		[1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1],
		[1,0,0,2,0,0,0,0,2,2,2,2,2,2,2,2,1],
		[1,0,1,2,1,0,1,0,1,2,1,2,1,2,1,2,1],
		[1,0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,1],
		[1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1],
		[1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1],
		[1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1],
		];

		mapWidth = map[0].length;
		mapHeight = map.length;
		
		bg = new Stamp(new BitmapData(HXP.width, Std.int(HXP.halfHeight), false, 0x88ff88), 0, Std.int(HXP.halfHeight));
		addGraphic(bg);
		layer = 400;
		
		twoPI = Math.PI * 2;
		stripWidth = 2;
		numRays = Math.floor(HXP.width / stripWidth);
		fov = 120 * HXP.RAD;
		fovHalf = fov / 2;
		viewDist = 255;
		
		initSpriteMap();
	}
	
	function initSpriteMap() 
	{
		//strips
		strips = new Array<Entity>();
		for (i in 0 ...numRays) {
			var img:Image = new Image(GameScene.texturesCache, new Rectangle(0, 0, 2, 64));
			img.scaledWidth = stripWidth;
			var strip = new Entity(i * 2, 0, img);
			strips.push(strip);
			scene.add(strip); 
		}
		
		// objects map
		objectsMap = new Array<Array<Entity>>();
		objectsMap = [];
		for (y in 0 ... map.length)
			objectsMap[y] = [];
		/*
		addBomb(2, 5);
		var fout = File.write("out.txt", false);
		fout.writeString("");
		fout.close();
		*/
	}
	
	override public function update() {
		clearGraphics();
		castRays();

		super.update();				
	}

	function castRays() {
		var stripIdx = 0;
		
		for (i in 0...numRays) {
			// where on the screen does ray go through?
			var rayScreenPos = (-numRays/2 + i) * stripWidth;
			// the distance from the viewer to the point on the screen, simply Pythagoras.
			var rayViewDist = Math.sqrt(rayScreenPos*rayScreenPos + viewDist*viewDist);

			// the angle of the ray, relative to the viewing direction.
			// right triangle: a = sin(A) * c
			var rayAngle = Math.asin(rayScreenPos / rayViewDist);
			castSingleRay(GameScene.player.rot + rayAngle, stripIdx++); 	// add the players viewing direction to get the angle in world space
		}
	}
	
	inline function castSingleRay(rayAngle:Float, stripIdx:Int) {
		// make sure the angle is between 0 and 360 degrees
		rayAngle %= twoPI;
		if (rayAngle < 0) rayAngle += twoPI;

		// moving right/left? up/down? Determined by which quadrant the angle is in.
		var right = (rayAngle > twoPI * 0.75 || rayAngle < twoPI * 0.25);
		var up = (rayAngle < 0 || rayAngle > Math.PI);

		var wallType = 0;
		
		// only do this once
		var angleSin = Math.sin(rayAngle);
		var angleCos = Math.cos(rayAngle);

		var dist:Float = HXP.NUMBER_MAX_VALUE;  // the distance to the block we hit
		var xHit:Float = 0;
		var yHit:Float = 0;  // the x and y coord of where the ray hit the block
		
		var textureX:Float=0;  // the x-coord on the texture of the block, ie. what part of the texture are we going to render
		var wallX=0;  // the (x,y) map coords of the block
		var wallY=0;

		var wallIsHorizontal = false;
		
		// first check against the vertical map/wall lines
		// we do this by moving to the right or left edge of the block we're standing in
		// and then moving in 1 map unit steps horizontally. The amount we have to move vertically
		// is determined by the slope of the ray, which is simply defined as sin(angle) / cos(angle).

		var slope = angleSin / angleCos;  // the slope of the straight line made by the ray
		var dXVer = right ? 1 : -1;  // we move either 1 map unit to the left or right
		var dYVer = dXVer * slope;  // how much to move up or down

		var x = right ? Math.ceil(GameScene.player.x) : Math.floor(GameScene.player.x);  // starting horizontal position, at one of the edges of the current map block
		var y = GameScene.player.y + (x - GameScene.player.x) * slope;  // starting vertical position. We add the small horizontal step we just made, multiplied by the slope.

		while (x >= 0 && x < mapWidth && y >= 0 && y < mapHeight) {
			var wallX = Math.floor(x + (right ? 0 : -1));
			var wallY = Math.floor(y);
			
			// sprite checking code
			var e:Entity = objectsMap[wallY][wallX];
			if (e != null && e.visible == false)
				objectsMap[wallY][wallX].visible = true;
				
			// is this point inside a wall block?
			if (map[wallY][wallX] > 0) {
				var distX = x - GameScene.player.x;
				var distY = y - GameScene.player.y;
				dist = distX*distX + distY*distY;  // the distance from the player to this point, squared.

				wallType = map[wallY][wallX]; // we'll remember the type of wall we hit for later
				textureX = y % 1;	// where exactly are we on the wall? textureX is the x coordinate on the texture that we'll use later when texturing the wall.
				if (!right) textureX = 1 - textureX; // if we're looking to the left side of the map, the texture should be reversed

				xHit = x;  // save the coordinates of the hit. We only really use these to draw the rays on minimap.
				yHit = y;
				
				wallIsHorizontal = true;
				
				break;
			}
			x += dXVer;
			y += dYVer;
		}

		// horizontal run
		// now check against horizontal lines. It's basically the same, just "turned around".
		// the only difference here is that once we hit a map block, 
		// we check if there we also found one in the earlier, vertical run. We'll know that if dist != 0.
		// If so, we only register this hit if this distance is smaller.

		var slope = angleCos / angleSin;
		var dYHor = up ? -1 : 1;
		var dXHor = dYHor * slope;
		var y = up ? Math.floor(GameScene.player.y) : Math.ceil(GameScene.player.y);
		var x = GameScene.player.x + (y - GameScene.player.y) * slope;

		while (x >= 0 && x < mapWidth && y >= 0 && y < mapHeight) {
			var wallY = Math.floor(y + (up ? -1 : 0));
			var wallX = Math.floor(x);
			
			// sprite checking code
			var e:Entity = objectsMap[wallY][wallX];
			if (e != null && e.visible == false)
				objectsMap[wallY][wallX].visible = true;
			
			if (map[wallY][wallX] > 0) {
				var distX = x - GameScene.player.x;
				var distY = y - GameScene.player.y;
				var blockDist = distX*distX + distY*distY;
				if (blockDist < dist) {
					dist = blockDist;
					xHit = x;
					yHit = y;

					wallType = map[wallY][wallX];
					textureX = x % 1;
					if (up) textureX = 1 - textureX;
				}
				break;
			}
			x += dXHor;
			y += dYHor;
		}

		if (dist != 0) { 
			//drawRay(xHit, yHit);
			dist = Math.sqrt(dist);

			// use perpendicular distance to adjust for fish eye
			// distorted_dist = correct_dist / cos(relative_angle_of_ray)
			dist = dist * Math.cos(GameScene.player.rot - rayAngle);

			// now calc the position, height and width of the wall strip

			// "real" wall height in the game world is 1 unit, the distance from the player to the screen is viewDist,
			// thus the height on the screen is equal to wall_height_real * viewDist / dist

			var height:Float = viewDist / dist;
			// top placement is easy since everything is centered on the x-axis, so we simply move
			// it half way down the screen and then half the wall height back up.
			var top = Math.round((HXP.height - height)) >> 1; // /2
			
			var texX = Math.floor(textureX * 64);
			if (texX > 64 - stripWidth)	// make sure we don't move the texture too far to avoid gaps.
				texX = Math.floor(64 - stripWidth);
			if (dist > 5 )
				texX += 64;
				
			var texY = (wallType-1) << 6; // *64
			
			var dwx = xHit - GameScene.player.x;
			var dwy = yHit - GameScene.player.y;
			var wallDist = dwx*dwx + dwy*dwy;
			
			var img:Image = cast(strips[stripIdx].graphic, Image);
			img.clipRect.left = texX; // new Rectangle(texX, texY, 2, 64);
			img.clipRect.top = texY; 
			img.clipRect.width = 2; 
			img.clipRect.height = 64; 
			img.updateBuffer();
			
			img.scaledHeight = height;
			img.y = top;
			strips[stripIdx].layer = Math.floor(wallDist) + 10;
		}
	}
	

	
	function clearGraphics():Void 
	{
		//graphic = null;
		
		for (y in 0...mapHeight) {
			for (x in 0...mapWidth) {
				if(objectsMap[y][x] != null)
					objectsMap[y][x].visible = false;
			}
		}
	}
	
		
	public function addBomb(x:Float, y:Float) 
	{
		var bombX:Int = Math.floor(x);
		var bombY:Int = Math.floor(y);
		objectsMap[bombY][bombX] = new Bomb(bombX, bombY);
		scene.add(objectsMap[bombY][bombX]);
	}
	
	/**
	 * Get sprite info based on its position
	 * @param	x : sprite X in tiles
	 * @param	y : sprite Y in tiles
	 * @return : infos to be drawn (array of Float) 
	 * [0] : sprite X
	 * [1] : sprite Y
	 * [2] : sprite size
	 * [3] : sprite layer
	 */
	public function getPos(x:Float, y:Float) : Array<Int>
	{	
		// translate position to viewer space
		var dx = x + 0.5 - GameScene.player.x;
		var dy = y + 0.5 - GameScene.player.y;
		var dbx = x - GameScene.player.x;
		var dby = y - GameScene.player.y;

		// distance to sprite
		var d = dx * dx + dy * dy;
		var dist = Math.sqrt(d);

		// sprite angle relative to viewing angle
		var spriteAngle = Math.atan2(dy, dx) - GameScene.player.rot;

		// size of the sprite
		var size = Std.int(GameScene.rayCast.viewDist / (Math.cos(spriteAngle) * dist));
	
		// x-position on screen
		var xpos = Math.tan(spriteAngle) * GameScene.rayCast.viewDist;
		
		// sprite pos
		var imgx = Std.int(HXP.halfWidth + xpos - size/2);
		// y is constant since we keep all sprites at the same height and vertical position
		var imgy = Std.int((HXP.height - size) / 2);
		
		/*
		var dbx = x - GameScene.player.x;
		var dby = y - GameScene.player.y;
		var d = dbx * dbx + dby * dby;
		*/
		
		var layer = Math.floor(d)+10;
		
		/*
		// write something
		var fout = File.append("out.txt", false);
		fout.writeString("bomb:"+ layer + " d:" + d + " dx:" + dx+ " dy:" + dy+ "\n");
		fout.close();
		//System.exit(0);
		*/
		var pos:Array<Int> = [imgx, imgy, size, layer];
				
		return pos;
	}
}
