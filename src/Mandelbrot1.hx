package src;

import com.haxepunk.Entity;
import flash.display.Bitmap;
import flash.display.BitmapData;
import flash.geom.Matrix;
import flash.geom.Rectangle;
import flash.Lib;
import flash.text.TextField;
import flash.utils.ByteArray;
import flash.utils.Timer;
import flash.Memory; // nme.memory

/**
 * ...
 * @author Godjam - QilinEggs
 */
class Mandelbrot1 extends Entity
{
	// from : http://stackoverflow.com/questions/10157787/haxe-nme-fastest-method-for-per-pixel-bitmap-manipulation
    public var pixels:Array<Array<Int>>;

    public var colorModifier:Int;
    private var bitmapData:BitmapData;
    private var bigBitmapData:BitmapData;

    private var fps:TextField;
    private var matrix:Matrix;
	
	
	public function new() 
    {
		super(0, 0);
        width = 400;// 320; //Std.int(flash.Lib.current.stage.stageWidth/2);
        height = 240; //Std.int(flash.Lib.current.stage.stageHeight/2);

        var scale:Float = 2;//flash.Lib.current.stage.stageWidth/width;
        matrix = new Matrix();
        matrix.scale(scale, scale);

        var setBitmap:Bitmap = new Bitmap();
        bitmapData = new BitmapData( width , height , false , 0x000000 );
        bigBitmapData = new BitmapData( Lib.current.stage.stageWidth , Lib.current.stage.stageHeight , false , 0x000000 );

        setBitmap.bitmapData = bigBitmapData;

        Lib.current.addChild( setBitmap );

        var maxIterations:Int = 128;

        pixels = new Array();


        var beforeTime = Lib.getTimer();
        generateFractal1();
        var afterTime = Lib.getTimer();

        var tf = new TextField();
        tf.width = 400;
        tf.text = "Generating fractal took "+(afterTime-beforeTime)+" ms";
        Lib.current.addChild(tf);

        fps = new TextField();
        fps.width = 400;
        fps.y = 10;
        fps.text = "FPS: ";
        Lib.current.addChild(fps);

        colorModifier = 2;
		/*
		var timer:Timer = new Timer(10);

        runLoop();
        timer.run = runLoop;
		*/
    }

    override public function update() 
	{
        var beforeTime = Lib.getTimer();
		renderLoop1();	
		
		// update color cycle
		colorModifier += 2;
        if(colorModifier > 65530)
                colorModifier = 0;
				
		var afterTime = Lib.getTimer();
        fps.text = "FPS: "+Math.round(1000/(afterTime-beforeTime));
    }
	
	// "slow" version of fractal generation 
	function generateFractal1():Void 
	{
		var xtemp;
		var iteration;
		var x0:Float = 0;
		var y0:Float = 0;
		for(ix in 0...width) {
			pixels[ix] = new Array();
			for(iy in 0...height) {
				x0 = 0;
				y0 = 0;
				iteration = 128;

				while ( x0*x0 + y0*y0 <= 4  &&  iteration > 0 ) 
				{
					xtemp = x0*x0 - y0*y0 + (ix-14*5000)/50000;
					y0 = 2*x0*y0 + (iy-(height/0.6))/50000;
					x0 = xtemp;

					iteration--;
				}

				pixels[ix][iy] = iteration;
			}
		}
	}
	
	// "slow" version of color rendering 
	function renderLoop1():Void 
	{
		var r:Int=0, b:Int=0, g:Int=0;
		var pixel:Int = 0;
		
		for(iy in 0...height) {
			for(ix in 0...width) {
				pixel = pixels[ix][iy];
				r = pixel + colorModifier;
				g = pixel + colorModifier + r;
				b = pixel + colorModifier + g;
				bitmapData.setPixel(ix, iy, (r<<16 | g<<8 | b));
			}
		}
	
		bigBitmapData.draw(bitmapData, matrix, null, null, null, false);
	
	}
}