package src;

import com.haxepunk.HXP;
import flash.display.BitmapData;
import com.haxepunk.utils.Draw;
import com.haxepunk.graphics.Stamp;
import com.haxepunk.Entity;

/**
 * ...
 * @author Godjam - QilinEggs
 */
class Minimap extends Entity
{
	var miniMapScale:Int;  // how many pixels to draw a map block
	public function new() 
	{
		super(x, y);
		
		miniMapScale = 4;
	}
	
	override public function render() { //TODO to cache if no modification
		var mapWidth = GameScene.rayCast.mapWidth;
		var mapHeight = GameScene.rayCast.mapHeight;
		var x0 = HXP.width - mapWidth * miniMapScale;
		var y0 = HXP.height - mapHeight* miniMapScale;
		// draw the topdown view minimap
		var miniMap = new BitmapData(mapWidth * miniMapScale, mapHeight * miniMapScale);
		Draw.setTarget(miniMap);
		
		// loop through all blocks on the map
		for (y in 0...mapHeight) {
			for (x in 0...mapWidth) {
				// wall
				var wall = GameScene.rayCast.map[y][x];
				var color :Int = 0x555555;
				if (wall == 2)
					color = 0x999999;
				if(wall != 0) {
					Draw.rect(x * miniMapScale, y * miniMapScale, miniMapScale, miniMapScale, color, 1);
				}
				
				//bomb
				var bomb = GameScene.rayCast.objectsMap[y][x];
				if (bomb != null)
					Draw.rect(x * miniMapScale+1, y * miniMapScale+1, miniMapScale-2, miniMapScale-2, 0xff0000, 1);
			}
		}
		
		var x1 = Math.round(GameScene.player.x * miniMapScale);
		var y1 = Math.round(GameScene.player.y * miniMapScale);
		var x2 = Math.round(x1 + Math.cos(GameScene.player.rot) * miniMapScale);
		var y2 = Math.round(y1 + Math.sin(GameScene.player.rot) * miniMapScale);
		Draw.line(x1, y1, x2, y2, 0x0); 
		
		graphic = new Stamp(miniMap, x0, y0);
		layer = 0; // top layer
		super.render();
	}
	
	public function drawRay(xHit:Float, yHit:Float) {
		var x1 = Math.round(GameScene.player.x * miniMapScale);
		var y1 = Math.round(GameScene.player.y * miniMapScale);
		var x2 = Math.round(xHit * miniMapScale);
		var y2 = Math.round(yHit * miniMapScale);
		Draw.line(x1, y1, x2, y2, 0x888888); 
	}
	
}