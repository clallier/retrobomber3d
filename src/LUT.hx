package src;

/**
 * ...
 * @author Godjam - QilinEggs
 */
class LUT
{
	/** Table of function values*/
	public static var sqrtTable:Array<Float>;
	/** Table of trig function values*/
	public static var cosTable:Array<Float>;
	public static var sinTable:Array<Float>;
	public static var asinTable:Array<Float>;


	/** 10^decimals of precision*/
	public static var sqrtPow:Float;
	/** 10^decimals of precision*/
	public static var pow:Float;

	/** 2 * PI, the number of radians in a circle*/
	public static inline var TWO_PI:Float = 6.28319;

	public function new () {
		generateSqrtTable(1, 100000);
		generateTrigTables(100);
	}
	
	/**
	*   Make the look up table
	*   @param max Maximum value to cache
	*   @param numDigits Number of digits places of precision
	*   @param func Function to call to generate stored values.
	*               Must be valid on [0,max).
	*   @throws Error If func is null or invalid on [0,max)
	*/
	public function generateSqrtTable(numDigits:UInt, max:Int)
	{
		sqrtPow = Math.pow(10, numDigits);
		var round:Float = 1.0 / sqrtPow;
		var len:UInt = 1 + Math.floor(max*sqrtPow);
		sqrtTable = new Array<Float>();

		var val:Float = 0;
		for (i in  0 ... len)
		{
			sqrtTable.push( Math.sqrt(val));
			val += round;
		}
	}
	
	public function generateTrigTables(numDigits:UInt)
	{
		pow = Math.pow(10, numDigits);
		var round:Float = 1.0 / pow;
		var len:UInt = 1 + Math.round(LUT.TWO_PI*pow);
		cosTable = new Array<Float>();
		sinTable = new Array<Float>();
		asinTable = new Array<Float>();

		var theta:Float = 0;
		for (i in 0 ... len)
		{
			cosTable.push(Math.cos(theta));
			sinTable.push(Math.sin(theta));
			asinTable.push(Math.asin(theta));
			theta += round;
		}
	}
 

	/**
	*   Look up the value of the given input
	*   @param val Input value to look up the value of
	*   @return The value of the given input
	*/
	public static inline function sqrt(val:Float): Float
	{
		return sqrtTable[Std.int(val*sqrtPow)];
	}
	
	public static inline function cos(rad:Float): Float
	{
		return rad>= 0
				? cosTable[Std.int((rad%TWO_PI)*pow)]
				: cosTable[Std.int((TWO_PI+rad%TWO_PI)*pow)];
	}
	
	public static inline function sin(rad:Float): Float
	{
		return sinTable[Std.int(rad*pow)];
	}
	
	public static inline function asin(rad:Float): Float
	{
		return asinTable[Std.int(rad*pow)];
	}
}