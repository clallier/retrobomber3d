package src;

import com.haxepunk.Entity;
import com.haxepunk.HXP;
import src.RayCast;
import com.haxepunk.graphics.Image;
import flash.geom.Rectangle; 

/**
 * ...
 * @author Godjam - QilinEggs
 */
class Bomb extends Entity
{
	// time
	var time:Float = 3;
	
	public function new(x:Int, y:Int) 
	{	
		super(x, y);
	}
	
	override public function update() 
	{
		time -= HXP.elapsed;
		
		if (time < 0)
			explode();
	}
	
	public function explode() 
	{
		var x:Int = Std.int(this.x);
		var y:Int = Std.int(this.y);
		
		// destroy itself
		scene.remove(this);
		GameScene.rayCast.objectsMap[y][x] = new Smoke(x, y);
		scene.add(GameScene.rayCast.objectsMap[y][x]);
		
		//destroy walls
		var w:Int; 
		w = GameScene.rayCast.map[y - 1][x];
		if (w == 2) GameScene.rayCast.map[y - 1][x] = 0;
		
		w = GameScene.rayCast.map[y + 1][x];
		if (w == 2) GameScene.rayCast.map[y + 1][x] = 0;
		
		w = GameScene.rayCast.map[y][x - 1];
		if (w == 2) GameScene.rayCast.map[y][x - 1] = 0;
		
		w = GameScene.rayCast.map[y][x + 1];
		if (w == 2) GameScene.rayCast.map[y][x + 1] = 0;
		
		//destoy neighbors
		destroyNeighbor(y - 1, x);
		destroyNeighbor(y + 1, x);
		destroyNeighbor(y, x + 1);
		destroyNeighbor(y, x - 1);
	}
	
	override public function render()
	{
		if (!visible) return;
		
		var pos:Array<Int> = GameScene.rayCast.getPos(x, y);
		var spriteX = pos[0];
		var spriteY = pos[1];
		var size = pos[2];
		var l = pos[3];
		
		if (size <= 0) return;
	
		// set graphic
		var left:Int = 0;
		if (time < 1) left = 64; 
		var img:Image = new Image(GameScene.texturesCache, new Rectangle(left, 2*64, 64, 64));
		img.scaledWidth = size;
		img.scaledHeight = size;
		img.x = spriteX;
		img.y = spriteY;
		graphic = img;
		
		layer = l;
		
		super.render();
	}
	
	public function destroyNeighbor(y:Int, x:Int ) {
		
		var e:Entity = GameScene.rayCast.objectsMap[y][x];
		var tile:Int = GameScene.rayCast.map[y][x];
		if (e == null && tile == 0) {
			GameScene.rayCast.objectsMap[y][x] = new Smoke(x, y);
			scene.add(GameScene.rayCast.objectsMap[y][x]);
		}
		
		if (Std.is(e, Bomb))
			Reflect.callMethod(e, Reflect.field(e, "explode"), []);
	}
}