package src;

import com.haxepunk.Entity;
import com.haxepunk.HXP;
import com.haxepunk.utils.Input;
import com.haxepunk.utils.Key;
import flash.geom.Point;

/**
 * ...
 * @author Godjam - QilinEggs
 */
class Player extends Entity
{
	public var dir :Float;			// the direction that the player is turning, either -1 for left or 1 for right.
	public var rot :Float;			// the current angle of rotation
	public var speed :Float;		// is the playing moving forward (speed = 1) or backwards (speed = -1).
	public var moveSpeed :Float;	// how far (in map units) does the player move each step/update
	public var rotSpeed :Float;	// how much does the player rotate each step/update (in radians)
	
	public function new(x:Float, y:Float) 
	{
		super(x, y);
		dir = 0;
		rot = -Math.PI / 2;
		speed = 0;
		moveSpeed = 0.06;
		rotSpeed = 6 * HXP.RAD;
		bindKeys();
	}
	
	override public function update() {
		super.update();
		move();
	}
	
	function bindKeys():Void 
	{
		// defines left and right as arrow keys and ZQSD controls
		Input.define("left", 	[Key.LEFT, 	Key.Q]);
		Input.define("right", 	[Key.RIGHT, Key.D]);
		Input.define("up", 		[Key.UP, 	Key.Z]);
		Input.define("down", 	[Key.DOWN, 	Key.S]);
		Input.define("shoot", 	[Key.SPACE, Key.E]);
	}
	
	function move() 
	{
		speed = 0;
		dir = 0;
		
		if (Input.check("left"))
			dir = 1;
		if (Input.check("right"))
			dir = -1;
		
		if (Input.check("up")) {
			speed = 1;
		}
			
		if (Input.check("down")) {
			speed = -1;
		}
		
		if (Input.released("shoot")) {
			dropBomb();
		}
		
		var moveStep = speed * moveSpeed;	// player will move this far along the current direction vector

		rot += dir * rotSpeed; // add rotation if player is rotating (player.dir != 0)

		var newX = x + Math.cos(rot) * moveStep;	// calculate new player position with simple trigonometry
		var newY = y + Math.sin(rot) * moveStep;

		var pos:Point = checkCollision(x, y, newX, newY, 0.35);
		
		x = pos.x; // set new position
		y = pos.y;
	}
	
	function dropBomb() 
	{
		GameScene.rayCast.addBomb(x, y);
	}

	
	function checkCollision(fromX:Float, fromY:Float, toX:Float, toY:Float, radius:Float) 
	{
		var pos:Point = new Point(fromX, fromY);
		
		var mapHeight = GameScene.rayCast.mapHeight;
		var mapWidth = GameScene.rayCast.mapWidth;

		if (toY < 0 || toY >= mapHeight || toX < 0 || toX >= mapWidth)
			return pos;

		var blockX = Math.floor(toX);
		var blockY = Math.floor(toY);


		if (isBlocking(blockX,blockY)) {
			return pos;
		}

		pos.x = toX;
		pos.y = toY;

		var blockTop = isBlocking(blockX,blockY-1);
		var blockBottom = isBlocking(blockX,blockY+1);
		var blockLeft = isBlocking(blockX-1,blockY);
		var blockRight = isBlocking(blockX+1,blockY);

		if (blockTop && toY - blockY < radius) {
			toY = pos.y = blockY + radius;
		}
		if (blockBottom && blockY+1 - toY < radius) {
			toY = pos.y = blockY + 1 - radius;
		}
		if (blockLeft && toX - blockX < radius) {
			toX = pos.x = blockX + radius;
		}
		if (blockRight && blockX+1 - toX < radius) {
			toX = pos.x = blockX + 1 - radius;
		}

		// is tile to the top-left a wall
		if (isBlocking(blockX-1,blockY-1) && !(blockTop && blockLeft)) {
			var dx = toX - blockX;
			var dy = toY - blockY;
			if (dx*dx+dy*dy < radius*radius) {
				if (dx*dx > dy*dy)
					toX = pos.x = blockX + radius;
				else
					toY = pos.y = blockY + radius;
			}
		}
		// is tile to the top-right a wall
		if (isBlocking(blockX+1,blockY-1) && !(blockTop && blockRight)) {
			var dx = toX - (blockX+1);
			var dy = toY - blockY;
			if (dx*dx+dy*dy < radius*radius) {
				if (dx*dx > dy*dy)
					toX = pos.x = blockX + 1 - radius;
				else
					toY = pos.y = blockY + radius;
			}
		}
		// is tile to the bottom-left a wall
		if (isBlocking(blockX-1,blockY+1) && !(blockBottom && blockBottom)) {
			var dx = toX - blockX;
			var dy = toY - (blockY+1);
			if (dx*dx+dy*dy < radius*radius) {
				if (dx*dx > dy*dy)
					toX = pos.x = blockX + radius;
				else
					toY = pos.y = blockY + 1 - radius;
			}
		}
		// is tile to the bottom-right a wall
		if (isBlocking(blockX+1,blockY+1) && !(blockBottom && blockRight)) {
			var dx = toX - (blockX+1);
			var dy = toY - (blockY+1);
			if (dx*dx+dy*dy < radius*radius) {
				if (dx*dx > dy*dy)
					toX = pos.x = blockX + 1 - radius;
				else
					toY = pos.y = blockY + 1 - radius;
			}
		}

		return pos;
	}
		
	function isBlocking(x:Float, y:Float) {
			
		var mapHeight = GameScene.rayCast.mapHeight;
		var mapWidth = GameScene.rayCast.mapWidth;
		var map = GameScene.rayCast.map;
		
		// first make sure that we cannot move outside the boundaries of the level
		if (y < 0 || y >= mapHeight || x < 0 || x >= mapWidth)
			return true;
		// return true if the map block is not 0, ie. if there is a blocking wall.
		return (map[Math.floor(y)][Math.floor(x)] != 0); 
	}
	
}