package src;

import com.haxepunk.Scene;
import flash.display.BitmapData;
import flash.display.StageQuality;
import openfl.Assets; 
import com.haxepunk.HXP;
import com.haxepunk.RenderMode;

/**
 * ...
 * @author Godjam - QilinEggs
 */
class GameScene extends Scene
{

	public static var texturesCache(default, null):BitmapData;
	public static var rayCast(default, null):RayCast;
	public static var player(default, null):Player;
	public static var minimap(default, null):Minimap;
	
	override public function begin() 
	{
		super.begin();
		texturesCache = Assets.getBitmapData("gfx/blocks.png");
		
		rayCast = new RayCast();
		add(rayCast);
		
		player = new Player(1.5, 8.5);
		add(player);
		
		minimap = new Minimap();
		add(minimap);
		//add(new Mode7());
		//add(new Mandelbrot1());
		//add(new Mandelbrot2());
	
		
		HXP.stage.quality = StageQuality.LOW;	}
}