package src;

import com.haxepunk.Entity;
import com.haxepunk.graphics.Emitter;
import com.haxepunk.math.Vector;
import flash.display.BitmapData;
import flash.geom.Point;
import flash.geom.Rectangle;
import com.haxepunk.HXP;
import com.haxepunk.graphics.Image;
import com.haxepunk.utils.Draw;

/**
 * ...
 * @author Godjam - QilinEggs
 */
class Smoke extends Entity
{
	var time:Float = 0.3;
	var img:Image;
	
	public function new(x:Int, y:Int) 
	{
		super(x, y);
		var left = HXP.rand(2) * 64;
		img = new Image(GameScene.texturesCache, new Rectangle(left, 3 * 64, 64, 64));
	}
	
	override public function update() 
	{
		time -= HXP.elapsed;
		
		if (time < 0) {
			var x:Int = Std.int(x);
			var y:Int = Std.int(y);
			scene.remove(this);
			GameScene.rayCast.objectsMap[y][x] = null;
		}
	}
	
	override public function render()
	{
		if (!visible) return;
		
		var pos:Array<Int> = GameScene.rayCast.getPos(x, y);
		var spriteX = pos[0];
		var spriteY = pos[1];
		var size = pos[2];
		var l = pos[3];
		
		if (size <= 0) return;
		
		img.scaledWidth = size;
		img.scaledHeight = size;

		img.x = spriteX;
		img.y = spriteY;
		
		graphic = img;
		layer = l;
		
		super.render();
	}
}