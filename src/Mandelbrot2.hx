package src;

import com.haxepunk.Entity;
import flash.display.Bitmap;
import flash.display.BitmapData;
import flash.geom.Matrix;
import flash.geom.Rectangle;
import flash.text.TextField;
import flash.utils.ByteArray;
import flash.Lib;
import flash.Memory;


/**
 * ...
 * @author Godjam - QilinEggs
 */
class Mandelbrot2 extends Entity
{

	// from : http://stackoverflow.com/questions/10157787/haxe-nme-fastest-method-for-per-pixel-bitmap-manipulation
		/*
	 * Look into the nme.Memory API. The idea is to create a ByteArray with the correct size (or get it from a BitmapData), select it as the current virtual memory space and manipulate its bytes directly.
	 * You'll get an approximately 10x speed boost with Flash and it should be way faster with the CPP target too. Don't forget to compile in Release mode or method inlining will be disabled and performances will suffer a lot.
	 *
	 * Basic usage example (untested code) :
	 */
	
	/* explations :
	var rect:Rectangle = bitmapData.rect;

	// 32bits integer = 4 bytes
	var size:Int = bitmapData.width * bitmapData.height * 4;

	// The virtual memory space we'll use
	var pixels:ByteArray = new ByteArray();

	// CPP does not support setting the length property directly
	#if (cpp) 
		pixels.setLength(size);
	#else 
		pixels.length = size; 
	#end

	// Select the memory space (call it once, not every frame)
	Memory.select(pixels);

	// And in your loop set your color
	// Color is in BGRA mode, nme.Memory can only be used in little endian mode.
	Memory.setI32((y * width + x) * 4, color);

	// When you're done, render the BitmapData
	// (don't forget to reset the ByteArray position)
	pixels.position = 0;
	bitmapData.setPixels(rect, pixels);
	*/
	
	// Keep in mind this is a very basic code example. In your case, you'd need to adapt it and actually use a double sized ByteArray because you need to store the iteration count too. 
	// Nested loops can be optimized in your main loop and you can avoid a lot of extra index/address computations :

	
	// First part of the ByteArray will be used to store the iteration count,
	// the second part to draw the pixels.
	
	// img to render size
	var rect:Rectangle;
	
	// 32bits integer = 4 bytes
	var size:Int;
	
	// The virtual memory space we'll use
	var pixels:ByteArray;
	
    public var colorModifier:Int;
    private var bitmapData:BitmapData;
    private var bigBitmapData:BitmapData;

    private var fps:TextField;
    private var matrix:Matrix;
	
	public function new() 
    {
		super(0, 0);
        width = 400;// 320; //Std.int(flash.Lib.current.stage.stageWidth/2);
        height = 240; //Std.int(flash.Lib.current.stage.stageHeight/2);

        var scale:Float = 2;//flash.Lib.current.stage.stageWidth/width;
		matrix = new Matrix();
        matrix.scale(scale, scale);
		var setBitmap:Bitmap = new Bitmap();
        bitmapData = new BitmapData( width , height , false , 0x000000 );
		bigBitmapData = new BitmapData( Lib.current.stage.stageWidth , Lib.current.stage.stageHeight , false , 0x000000 );

        setBitmap.bitmapData = bigBitmapData;
        Lib.current.addChild( setBitmap );
		
		// img to render size
		rect = bitmapData.rect;
		
		// 32bits integer = 4 bytes
		size = bitmapData.width * bitmapData.height * 4;
		
		// The virtual memory space we'll use
		pixels = new ByteArray();
		
		// Note the size * 2 !
		#if (cpp) 
			pixels.setLength(size * 2);
		#else 
			pixels.length = size * 2; 
		#end
		
		Memory.select(pixels);
		
        var maxIterations:Int = 128;

        var beforeTime = Lib.getTimer();
        generateFractal2();
        var afterTime = Lib.getTimer();

        var tf = new TextField();
        tf.width = 400;
        tf.text = "Generating fractal took "+(afterTime-beforeTime)+" ms";
        Lib.current.addChild(tf);

        fps = new TextField();
        fps.width = 400;
        fps.y = 10;
        fps.text = "FPS: ";
        Lib.current.addChild(fps);

        colorModifier = 2;
    }

    override public function update() 
	{
        var beforeTime = Lib.getTimer();
		renderLoop2();	
		
		// update color cycle
		colorModifier += 2;
        if(colorModifier > 65530)
                colorModifier = 0;
				
		var afterTime = Lib.getTimer();
        fps.text = "FPS: "+Math.round(1000/(afterTime-beforeTime));
    }
	
	// "fast" version of fractal rendering
	function generateFractal2():Void 
	{
		// First loop storing iteration count
		var xtemp;
		var iteration:Int = 0;
		var x0:Float = 0;
		var y0:Float = 0;
		
		for (iy in 0...height)
		{
			for (ix in 0...width)
			{
				x0 = 0;
				y0 = 0;
				iteration = 128;
	
				while ( x0*x0 + y0*y0 <= 4  &&  iteration > 0 ) 
				{
					xtemp = x0*x0 - y0*y0 + (ix-14*5000)/50000;
					y0 = 2*x0*y0 + (iy-(height/0.6))/50000;
					x0 = xtemp;
	
					iteration--;
				}
				
				Memory.setI32(((iy * width) + ix) << 2, iteration);
			}
		}
	}
	
	// "fast" version of the color rendering
	function renderLoop2():Void 
	{
		var r:Int=0, b:Int=0, g:Int=0;
		var pixel:Int = 0;
		for (i in 0...(height * width))
		{
			// Get the iteration count
			pixel = Memory.getI32(i << 2);
	
			r = pixel + colorModifier;
			g = pixel + colorModifier + r;
			b = pixel + colorModifier + g;
	
			// Note that we're writing the pixel in the second part of our ByteArray
			Memory.setI32(size + (i << 2), r | g << 8 | b << 16);
		}
	
		// Sets the position to the second part of our ByteArray
		pixels.position = size;
		bitmapData.setPixels(rect, pixels);
		//And this is it. If you really don't want to use Alchemy Opcodes on the Flash target, the next fastest way to blit pixels is to use getVector() / setVector() from the BitmapData class. But it's really not as fast.
		
		// x2
		bigBitmapData.draw(bitmapData, matrix, null, null, null, false);
	}
	
}