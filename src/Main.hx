package ;

import flash.display.Sprite;
import flash.events.Event;
import flash.Lib;
import com.haxepunk.HXP;
import src.GameScene;
import com.haxepunk.Engine;


/**
 * ...
 * @author Godjam - QilinEggs
 */

class Main extends Engine 
{
	var inited:Bool;

	/* ENTRY POINT */
	
	override function init() 
	{
		if (inited) return;
		inited = true;

		// (your code here)
		super.init();
		HXP.scene = new GameScene();
		
#if debug
		HXP.console.enable();
#end
	}

	/* SETUP */
	
	public static function main() 
	{
		// static entry point
		Lib.current.stage.align = flash.display.StageAlign.TOP_LEFT;
		Lib.current.stage.scaleMode = flash.display.StageScaleMode.NO_SCALE;
		Lib.current.addChild(new Main());
	}
}
