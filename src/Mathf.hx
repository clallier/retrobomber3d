package src;

/**
 * ...
 * @author Godjam - QilinEggs
 */
class Mathf
{
	// from http://riven8192.blogspot.fr/
   static inline var BIG_ENOUGH_INT:Int   = 16 * 1024;
   static inline var BIG_ENOUGH_FLOOR:Float = BIG_ENOUGH_INT + 0.0000;
   static inline var BIG_ENOUGH_ROUND:Float = BIG_ENOUGH_INT + 0.5000;
   static inline var BIG_ENOUGH_CEIL:Float  = BIG_ENOUGH_INT + 0.9999;

   public inline static function floor(x:Float):Int {
      return Std.int(x + BIG_ENOUGH_FLOOR) - BIG_ENOUGH_INT;
   }

   public inline static function round(x:Float):Int {
      return Std.int((x + BIG_ENOUGH_ROUND) - BIG_ENOUGH_INT);
   }

   public inline static function ceil(x:Float):Int {
      return Std.int((x + BIG_ENOUGH_CEIL) - BIG_ENOUGH_INT);
   }
}