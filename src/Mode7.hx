package src;

import com.haxepunk.Entity;
import com.haxepunk.graphics.Stamp;
import flash.display.Bitmap;
import flash.display.BitmapData;
import flash.display.Graphics;
import flash.display.Sprite;
import flash.geom.Point;
import haxe.macro.Expr.Function;
import openfl.Assets;
import com.haxepunk.HXP;

import com.haxepunk.utils.Input;
import com.haxepunk.utils.Key;

/**
 * Mode7 supernes test
 * @author Godjam - QilinEggs
 */
class Mode7 extends Entity
{
 
	// from : http://gamedev.stackexchange.com/questions/24957/doing-an-snes-mode-7-affine-transform-effect-in-pygame
	var timeout:Float = 1;
	var player:Player;
	var i:BitmapData;
	var data:BitmapData;

	var yres:Int = 200;
	var xres:Int = 300;
	var xMin:Int;
	var xMax:Int;
	var yMin:Int;
	var yMax:Int;
		

	public function new() 
	{
		super(0,0);
		
		player = new Player(150, 150, null);
		i = Assets.getBitmapData("gfx/StarCup1.png");
		data = new BitmapData(xres, yres);
		xMin = Math.floor( -xres / 2);
		xMax = Math.floor( xres / 2);
		yMin = Math.floor( -yres / 2);
		yMax = Math.floor( yres / 2);
	}
	
	override public function update() 
	{
		player.update();
		timeout -= HXP.elapsed;
		
		if(timeout < 0) {
			renderMode7();
			timeout = 1;
		}
		
		super.update();
	}
	
	function renderMode7():Void 
	{
		var horizon = -20; //adjust if needed
		var fov = 200; 
		var plx = Math.floor(player.x);
		var ply = Math.floor(player.y);
		var pla = player.rot;
	
		for (y in horizon ...yMax) {	
			for (x in xMin ...xMax) {
				var px = x;
				var py = y - horizon - fov; 
				var pz = y - horizon;      
	
				//projection 
				var sx = Math.floor(px / pz);
				var sy = Math.floor(py / pz);
				
				var	sxr = sx * Math.cos(pla) - sy * Math.sin(pla);
				var syr = sx * Math.sin(pla) + sy * Math.cos(pla);
	
				var scaling = 4; //adjust if needed, depends of texture size
				var color = i.getPixel(Math.floor(sxr * scaling+plx), Math.floor(syr * scaling+ply));  
	
				//put (color) at (x, y) on screen
				data.setPixel(x+150, y+100, color);
			}
		}
		graphic = new Stamp(data);
	}
	
}